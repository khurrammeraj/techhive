"use strict";

var should = require("should");

function sumNumbers(a, b) {
  return a + b;
}

function subtractNumbers(a, b) {
  return a - b;
}

function divideNumbers(a, b) {
  if (b != 0) {
    return a / b;
  } else {
    return null;
  }
}

function multiplyNumbers(a, b) {
  return a * b;
}

describe("Carrying out tests on some testable code", function() {
  it("should get the sum of 2 numbers", function() {
    sumNumbers(3, 4).should.be.exactly(7);
  });

  it("should get the difference of 2 numbers", function() {
    subtractNumbers(4, 3).should.be.exactly(1);
  });

  it("should get the product of 2 numbers", function() {
    multiplyNumbers(4, 3).should.be.exactly(12);
  });

  it("should get the division of 2 numbers", function() {
    divideNumbers(6, 2).should.be.exactly(3);
  });

  it("should get null when dividing by 0", function() {
    should.equal(divideNumbers(4, 0), null);
  });

  after(function() {
    console.log("Testing completed successfully.");
  });
});
